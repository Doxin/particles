module particledefs.air;

//NOTE: this particle never actually gets used, other than the color and id.

import particle: ParticleProperties;
import graphics: Pixel;

enum ParticleProperties properties={
    surfaceTension: 0,
    color:          Pixel(0, 0, 0),
    airDrag:        0,
    density:        0
};
