module particledefs.water;

import particle: ParticleProperties;
import graphics: Pixel;

enum ParticleProperties properties={
    surfaceTension: 0.7,
    //color:          Pixel(66, 125, 244),
    color:          Pixel(255,255,255),
    airDrag:        0.1,
    density:        1
};
