module particledefs.wall;

import particle: ParticleProperties;
import graphics: Pixel;

enum ParticleProperties properties={
    surfaceTension: 0,
    color:          Pixel(128, 128, 128),
    airDrag:        0,
    density:        float.infinity
};
