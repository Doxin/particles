module particledefs.dust;

import particle: ParticleProperties;
import graphics: Pixel;

enum ParticleProperties properties={
    surfaceTension: 0,
    color:          Pixel(255, 236, 163),
    airDrag:        1,
    density:        0.1
};
