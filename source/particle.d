import graphics: Pixel;
static import graphics;
import std.conv;
static import simulation;
import particles: particleType;

struct ParticleProperties{
    float surfaceTension;
    Pixel color;
    float airDrag;
    float density;
}

struct Particle{
    uint id;
    uint type;
    float x=0;
    float y=0;
    float xvel=0;
    float yvel=0;
    
    void destroy(){
        simulation.destroyParticle(this);
    }
    
    void render(){
        graphics.pixels[y.to!int][x.to!int]=color;
    }
    
    void unrender(){
        graphics.pixels[y.to!int][x.to!int]=Pixel(0,0,0);
    }
    
    float surfaceTension(){
        return particleType[type].surfaceTension;
    }
    
    Pixel color(){
        return particleType[type].color;
    }
    
    float airDrag(){
        return particleType[type].airDrag;
    }
    
    float density(){
        return particleType[type].density;
    }
}

string importParticles(string particleName)(){
    return "static import particledefs."~particleName~";\n";
}

string importParticles(string particleName, rest...)(){
    return importParticles!(particleName)~importParticles!(rest);
}

string appendParticles(string particleName)(){
    return "particledefs."~particleName~".properties, ";
}

string appendParticles(string particleName, rest...)(){
    return appendParticles!(particleName)~appendParticles!(rest);
}

string defineParticles(uint count, string particleName)(){
    return "enum "~particleName~" = "~count.to!string~";\n";
}

string defineParticles(uint count, string particleName, rest...)(){
    return defineParticles!(count, particleName)~defineParticles!(count+1, rest);
}

string loadParticles(particles...)(){
    string code = importParticles!(particles);
    code ~= "ParticleProperties[] particleType=[";
    code ~= appendParticles!(particles);
    code ~= "];\n";
    code ~= defineParticles!(0, particles);
    return code;
}
