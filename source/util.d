T sign(T)(T v){
    if(v>0){
        return 1;
    } else if(v<0){
        return -1;
    } else {
        return 0;
    }
}
