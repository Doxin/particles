import derelict.sdl2.sdl;

static import graphics;
static import simulation;
static import particles;
import particle: Particle;


void eventHandler(SDL_Event event){
    switch (event.type){
        case SDL_QUIT:
            graphics.quit();
            break;
        default: break;
    }
}

void main()
{
    enum size=16;
    enum step=3;
    /+for(int x=-size*step; x<size*step; x+=step){
        for(int y=-size*step; y<size*step; y+=step){
            simulation.addParticle(Particle(0, particles.water, 320+x, 240+y));
        }
    }+/
    
    for(int x=0; x<simulation.simWidth; x+=1){
        for(int y=-16; y<0; y+=1){
            simulation.addParticle(Particle(0, particles.wall, x, 480+y));
        }
    }
    
    graphics.mainLoop(&eventHandler, &simulation.step);
}
