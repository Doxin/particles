import std.conv;
import std.range: popBack;
import std.math: abs, sqrt, isNaN, isFinite;
import std.algorithm: max;
import std.format;
import std.stdio;

import particle:Particle;
static import particles;
import util: sign;

enum reservedParticles=100000;

struct ParticleList{
    Particle[reservedParticles] list;
    alias list this;
    size_t usedLength=0;
    
    invariant{
        assert(this.usedLength<reservedParticles);
    }
}

enum simWidth = 640;
enum simHeight = 480;
enum gravity = 10;
enum interactionRadius=5;
enum interactionRadius2=interactionRadius*interactionRadius;

Particle*[uint][simHeight][simWidth] particleGrid;
ParticleList particleList;
uint[] emptySlots;

void addParticle(Particle particle){
    assert(particle.type!=particles.air);
    
    if(emptySlots.length){
        particle.id=emptySlots[0];
        emptySlots.popBack;
        particleList[particle.id]=particle;
    } else {
        particle.id=particleList.usedLength.to!uint;
        particleList[particle.id]=particle;
        particleList.usedLength+=1; //TODO move all this logic into the particleList
    }

    particleGrid[particle.x.to!int][particle.y.to!int][particle.id]=&(particleList[particle.id]);
}

void destroyParticle(Particle particle){
    if(!(particle.x<0 || particle.x>=simWidth || particle.y<0 || particle.y>=simHeight)){
        particleGrid[particle.x.to!int][particle.y.to!int].remove(particle.id);
    }
    particleList[particle.id]=Particle(particle.id);
}

void removeParticleFromGrid(Particle particle){
    assert(particleGrid[particle.x.to!int][particle.y.to!int].remove(particle.id));
}

void addParticleToGrid(Particle particle){
    particleGrid[particle.x.to!int][particle.y.to!int][particle.id]=&(particleList[particle.id]);
}

void step(float dt){
    foreach(i, Particle particle; particleList[0 .. particleList.usedLength]){
        assert(particle.id==i, format!"ID mismatch, expected %d got %d"(i, particle.id));
        if(particle.type==particles.air){   
            continue;
        }
        // gravity
        particle.yvel+=gravity*dt;
        assert(!particle.xvel.isNaN);
        assert(particle.xvel.isFinite);
        assert(!particle.yvel.isNaN);
        assert(particle.yvel.isFinite);
        
        // air drag
        
        if(particle.density.isFinite){
            assert(!particle.density.isNaN);
            assert(particle.density.isFinite);
            assert(!particle.airDrag.isNaN);
            assert(particle.airDrag.isFinite);
            auto xDrag=(particle.density*(particle.xvel*particle.xvel))/2*particle.airDrag*dt;
            auto yDrag=(particle.density*(particle.yvel*particle.yvel))/2*particle.airDrag*dt;
            assert(!xDrag.isNaN);
            assert(xDrag.isFinite);
            assert(!yDrag.isNaN);
            assert(yDrag.isFinite);
            
            particle.xvel = max(abs(particle.xvel)-xDrag, 0)*sign(particle.xvel);
            particle.yvel = max(abs(particle.yvel)-yDrag, 0)*sign(particle.yvel);
            assert(!particle.xvel.isNaN);
            assert(particle.xvel.isFinite);
            assert(!particle.yvel.isNaN);
            assert(particle.yvel.isFinite);
        }
        
        //particle interaction
        for(int rx=-interactionRadius; rx<=interactionRadius; rx+=1){
            for(int ry=-interactionRadius; ry<=interactionRadius; ry+=1){
                auto x=particle.x.to!int+rx;
                auto y=particle.y.to!int+ry;
                
                if(x<0 || x>=simWidth || y<0 || y>=simHeight){
                    continue;
                }

                auto foo=particleGrid[x][y];
                
                foreach(Particle* particle2; particleGrid[x][y].byValue()){
                    if(particle.type==particles.air){   
                        continue;
                    }
                    
                    if(particle.density==float.infinity && particle2.density==float.infinity){
                        continue;
                    }
                    
                    auto xdiff=particle.x-particle2.x;
                    auto ydiff=particle.y-particle2.y;
                    
                    if(&particle==particle2){
                        continue;
                    }
                    
                    auto distance2=xdiff*xdiff+ydiff*ydiff;
                    if(distance2>interactionRadius2){
                        continue;
                    }
                    
                    if(distance2==0){
                        distance2=0.001;
                    }
                    
                    auto distance=sqrt(distance2);
                    auto it=distance/interactionRadius;
                    auto t=1-it;
                    assert(t>=0 && t<=1);
                    
                    //positive is attract
                    
                    auto long_force = t;
                    long_force = long_force*long_force;
                    
                    auto short_force = (t+0.13);
                    short_force = short_force*short_force*short_force;
                    
                    auto force=(long_force-short_force)*100;
                    
                    //auto force=1;
                    
                    
                    //TODO: ignore short_force for dissimilar materials
                    //TODO: keep into account density when distributing force
                    //TODO: make sure forces are only applied once instead of twice
                    
                    auto xvec=(particle.x-particle2.x)/distance*force*dt;
                    auto yvec=(particle.y-particle2.y)/distance*force*dt;
                    
                    float massPercentage;
                    if(particle.density==float.infinity){
                        massPercentage=0;
                    } else if(particle2.density==float.infinity){
                        massPercentage=1;
                    } else {
                        massPercentage=particle2.density/(particle.density+particle2.density);
                    }
                    
                    particle.xvel-=xvec*massPercentage;
                    particle.yvel-=yvec*massPercentage;
                    particle2.xvel+=xvec*(1-massPercentage);
                    particle2.yvel+=yvec*(1-massPercentage);
                }
            }
        }
        
        //stick updated particle back in list
        assert(particle.id<particleList.length);
        assert(particle.id==i);
        particleList[particle.id]=particle; //TODO: avoid copying shit
    }
    
    foreach(i, Particle particle; particleList){
        assert(particle.id==i, format!"ID mismatch, expected %d got %d"(i, particle.id));
        if(particle.type==particles.air){   
            continue;
        }
        
        //particle motion
        if(particle.xvel!=0 || particle.yvel!=0){
            particle.unrender();
            removeParticleFromGrid(particle);
            assert(!particle.xvel.isNaN);
            assert(particle.xvel.isFinite);
            assert(!particle.yvel.isNaN);
            assert(particle.yvel.isFinite);
            particle.x += particle.xvel;
            particle.y += particle.yvel;
            
            if(particle.x<0 || particle.x>=simWidth || particle.y<0 || particle.y>=simHeight){
                particle.destroy();
                continue;
            }
            
            addParticleToGrid(particle);
            
            //stick updated particle back in list
            assert(particle.id<particleList.length);
            particleList[particle.id]=particle;  //TODO: avoid copying shit
        }
        particle.render();
    }
}
