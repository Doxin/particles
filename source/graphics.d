import derelict.sdl2.sdl;

enum windowWidth=640;
enum windowHeight=480;

static this(){
	DerelictSDL2.load();
}

struct Pixel{
    ubyte b;
    ubyte g;
    ubyte r;
    ubyte a=255;
    
    static Pixel opCall(ubyte r, ubyte g, ubyte b){
        Pixel pixel;
        pixel.b=b;
        pixel.g=g;
        pixel.r=r;
        pixel.a=255;
        return pixel;
    }
    
    static Pixel opCall(){
        Pixel pixel;
        pixel.b=0;
        pixel.g=0;
        pixel.r=0;
        pixel.a=255;
        return pixel;
    }
}

Pixel[windowWidth][windowHeight] pixels;
private bool doQuit = false;

void mainLoop(void function(SDL_Event) eventHandler, void function(float) stepHandler){
    doQuit = false;

    SDL_Event event;

    SDL_Init(SDL_INIT_VIDEO);
    scope(exit) SDL_Quit();
    
    SDL_Window* window = SDL_CreateWindow("Particles", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, windowWidth, windowHeight, 0);
    scope(exit) SDL_DestroyWindow(window);
    
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);
    scope(exit) SDL_DestroyRenderer(renderer);
    
    SDL_Texture* texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, windowWidth, windowHeight);
    scope(exit) SDL_DestroyTexture(texture);
    
    uint frameTime;
    while (!doQuit){
        frameTime=SDL_GetTicks();
        while(SDL_PollEvent(&event)){
            eventHandler(event);
        }
        
        stepHandler(1.0f/60.0f); //TODO some fancy nonfixed fps stuff?
        
        SDL_UpdateTexture(texture, null, pixels.ptr, windowWidth * Pixel.sizeof);
        
        SDL_RenderClear(renderer); // optional?
        SDL_RenderCopy(renderer, texture, null, null);
        SDL_RenderPresent(renderer);
        SDL_Delay(16);
        import std.stdio;
        writeln("frame");
    }
}

void quit(){
    doQuit=true;
}
